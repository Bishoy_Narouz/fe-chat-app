import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './Routes';

import { LocalStorageModule } from 'angular-2-local-storage';

import { AppComponent } from './app.component';
import { LoginComponent } from './Features/Login/LoginComponent';
import { SignupComponent } from './Features/Register/RegisterComponent';
import { CreatePasswordComponent } from './Features/CreatePassword/CreatePasswordComponent';
import { ResendMailComponent } from './Features/ResendMail/ResendMailComponent';
import { ChatComponent } from './Features/Chat/ChatComponent';
import { Chat2Component } from './Features/Chat2/Chat2Component';

import { AccountRepository } from './Repositories/AccountRepository';
import { MessageRepository } from './Repositories/MessageRepository';
import { UserRepository } from './Repositories/UserRepository';
import { SocketService } from './Repositories/SocketRepository';

import { StorageService } from './Services/StorageService';
import { AuthGuard } from './Services/AuthGuardService';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    CreatePasswordComponent,
    ResendMailComponent,
    ChatComponent,
    Chat2Component
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    LocalStorageModule.forRoot({
      prefix: 'my-app',
      storageType: 'localStorage'
    })

  ],
  providers: [AccountRepository, MessageRepository, UserRepository, StorageService, AuthGuard, SocketService],
  bootstrap: [AppComponent]
})

export class AppModule { }
