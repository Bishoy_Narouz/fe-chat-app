import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './Features/Register/RegisterComponent';
import { CreatePasswordComponent } from './Features/CreatePassword/CreatePasswordComponent';
import { ResendMailComponent } from './Features/ResendMail/ResendMailComponent';
import { LoginComponent } from './Features/Login/LoginComponent';
import { ChatComponent } from './Features/Chat/ChatComponent';
import { Chat2Component } from './Features/Chat2/Chat2Component';
import { AuthGuard } from './Services/AuthGuardService';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: SignupComponent },
  { path: 'setPassword/:verificationCode', component: CreatePasswordComponent },
  { path: 'resend-mail', component: ResendMailComponent },
  {
    path: 'chat-area',
    component: ChatComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'chat',
    component: Chat2Component,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  { path: '**', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
