import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { StorageService } from './StorageService';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private storageService: StorageService, private _router: Router) { }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        let currentUser = this.storageService.GetCurrentUser();
        if (currentUser && currentUser["token"]) {
            return true;
        }

        this._router.navigate(['/login']);
        return false;
    }

}