import { Component, OnInit } from '@angular/core';
import { AccountRepository } from '../../Repositories/AccountRepository';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
    selector: 'create-password',
    templateUrl: './CreatePasswordComponent.html'
})
export class CreatePasswordComponent implements OnInit {
    errorMessage = '';
    isErrorMessage: any;

    user = {
        password: '',
        verificationCode: ''
    };

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private accountRepository: AccountRepository
    ) { }

    ngOnInit() {
        this.user.verificationCode = this.route.snapshot.paramMap.get("verificationCode");
    }

    Submit(frm: any) {
        if (frm.valid) {
            this.accountRepository.SetPassword(this.user).subscribe(
                response => {
                    if (response["Success"] === true) {
                        // this.router.navigateByUrl('/login');
                    } else {
                        if (response["Message"] === 'INVALID_VERIFICATION_CODE') {
                            this.isErrorMessage = 2;
                            this.errorMessage = 'Verification Code is not Valid';
                        } else {
                            this.isErrorMessage = 2;
                            this.errorMessage = 'Operation Failed';
                        }
                    }
                },
                err => {
                    this.isErrorMessage = 2;
                    this.errorMessage = 'Operation Failed';
                }
            )
        }
    }

}
