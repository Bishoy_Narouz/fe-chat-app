import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AccountRepository } from '../../Repositories/AccountRepository';
import { StorageService } from '../../Services/StorageService';

@Component({
    selector: 'login',
    templateUrl: './LoginComponent.html'
})
export class LoginComponent {
    errorMessage = '';

    user = {
        identity: '',
        password: ''
    }

    constructor(
        private router: Router,
        private accountRepository: AccountRepository,
        private storageService: StorageService
    ) { }

    Login(frm: any) {
        if (frm.valid) {
            this.accountRepository.Login(this.user).subscribe(
                res => {
                    if (res["Success"] === true) {
                        let user = res["Data"];
                        this.storageService.SetCurrentUser(user);
                        console.log(this.storageService.GetCurrentUser());
                        this.router.navigateByUrl('/chat');
                    } else {
                        if (res["Message"] === 'INVALID_CREDENTIALS') {
                            this.errorMessage = 'Invalide Email or Password';
                        } else if (res["Message"] === 'ACCOUNT_IS_NOT_VERIFIED') {
                            this.errorMessage = 'Account is not Verified';

                        } else {
                            this.errorMessage = 'Operation Failed';
                        }
                    }
                },
                err => {
                    this.errorMessage = 'Operation Failed';
                }
            )
        }
    }

}
