import { Component, OnInit, ElementRef, ViewChild, AfterViewChecked } from '@angular/core';
import { Router } from '@angular/router';
import { SocketService } from '../../Repositories/SocketRepository';
import { StorageService } from '../../Services/StorageService';
import { MessageRepository } from '../../Repositories/MessageRepository';

@Component({
    selector: 'chat2',
    templateUrl: './Chat2Component.html',
    styleUrls: ['./Chat2Component.css']
})
export class Chat2Component implements OnInit {
    currentUser: any;
    textMessage: any;
    Messages: any;
    messageObj: Object;
    container: HTMLElement;

    constructor(
        private router: Router,
        private storageService: StorageService,
        private socketService: SocketService,
        private messageRepository: MessageRepository
    ) {
        this.messageRepository.GetAllMessages().subscribe(
            res => {
                this.Messages = res["Data"];
            }
        );
    }

    ngOnInit() {
        this.currentUser = this.storageService.GetCurrentUser();

        this.socketService
            .getMessages()
            .subscribe((messageObj: Object) => {
                this.Messages.push(messageObj);
            });
    }

    sendMessage() {
        if (this.textMessage) {
            this.messageObj = {
                userId: this.currentUser._id,
                username: this.currentUser.username,
                textMessage: this.textMessage,
                messageDate: new Date()
            };
            this.socketService.sendMessage(this.messageObj);
            this.container = document.getElementById("chatHistory");
            this.textMessage = '';
        }
    }

    LogOut() {
        this.storageService.RemoveCurrentUser();
        this.router.navigateByUrl('/login');
    }

    AdvancedUIChat() {
        this.router.navigateByUrl('/chat-area');
    }
}