import { Component, OnInit, ElementRef, ViewChild, AfterViewChecked } from '@angular/core';
import { Router } from '@angular/router';
import { SocketService } from '../../Repositories/SocketRepository';
import { StorageService } from '../../Services/StorageService';
import { MessageRepository } from '../../Repositories/MessageRepository';
import { UserRepository } from '../../Repositories/UserRepository';

@Component({
    selector: 'chat',
    templateUrl: './ChatComponent.html',
    styleUrls: ['./ChatComponent.css']
})
export class ChatComponent implements OnInit, AfterViewChecked {
    @ViewChild('scrollMe') private myScrollContainer: ElementRef;

    Friends: any;
    currentUser: any;
    textMessage: any;
    Messages: any;
    messageObj: Object;
    container: HTMLElement;

    constructor(
        private router: Router,
        private userRepository: UserRepository,
        private storageService: StorageService,
        private socketService: SocketService,
        private messageRepository: MessageRepository
    ) {
        this.messageRepository.GetAllMessages().subscribe(
            res => {
                this.Messages = res["Data"];
                // console.log(this.Messages);
            }
        );
    }

    ngAfterViewChecked() {
        this.scrollDown();
    }

    ngOnInit() {
        this.currentUser = this.storageService.GetCurrentUser();
        this.userRepository.FindAllFriends(this.currentUser._id).subscribe(
            res => {
                this.Friends = res["Data"];
                // console.log(this.Friends);
            }
        );

        this.socketService
            .getMessages()
            .subscribe((messageObj: Object) => {
                // console.log(messageObj);
                this.Messages.push(messageObj);
                // console.log(this.Messages);
            });

    }

    sendMessage() {
        if (this.textMessage) {
            this.messageObj = {
                userId: this.currentUser._id,
                username: this.currentUser.username,
                textMessage: this.textMessage,
                messageDate: new Date()
            };
            this.socketService.sendMessage(this.messageObj);
            this.container = document.getElementById("chatHistory");
            this.textMessage = '';
            this.scrollDown();
        }
    }

    scrollDown() {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    }

    LogOut() {
        this.storageService.RemoveCurrentUser();
        this.router.navigateByUrl('/login');
    }
}
