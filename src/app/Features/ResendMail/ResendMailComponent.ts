import { Component } from '@angular/core';
import { AccountRepository } from '../../Repositories/AccountRepository';
import { Router } from '@angular/router';

@Component({
    selector: 'resend-mail',
    templateUrl: './ResendMailComponent.html'
})
export class ResendMailComponent {
    userEmail = '';
    errorMessage = '';
    isErrorMessage: any;

    constructor(
        private router: Router,
        private accountRepository: AccountRepository
    ) { }

    ResendMail(frm: any) {
        if (frm.valid) {
            this.accountRepository.ResendVerificationEmail(this.userEmail).subscribe(
                res => {
                    if (res["Success"] === true) {
                        this.isErrorMessage = 3;
                        this.errorMessage = 'Mail has been sent, Please check your Email ';
                    } else {
                        if (res["Message"] === 'INVALID_EMAIL') {
                            this.isErrorMessage = 2;
                            this.errorMessage = 'Email is not Found';
                        } else if (res["Message"] === 'VERIFIED_ACCOUNT') {
                            this.isErrorMessage = 2;
                            this.errorMessage = 'Account is verified Please Login';
                        }
                        else {
                            this.isErrorMessage = 2;
                            this.errorMessage = 'Operation Failed';
                        }
                    }
                }, err => {
                    this.isErrorMessage = 2;
                    this.errorMessage = 'Operation Failed';
                }
            )
        }
    }

}
