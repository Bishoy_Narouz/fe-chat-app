import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class MessageRepository {

    constructor(
        private http: HttpClient,
    ) { }

    hostUrl: string = 'http://localhost:5000/api/';
    apiUrl: string = this.hostUrl + 'Message/';

    GetAllMessages() {
        let url = this.apiUrl + "GetAllMessages";
        return this.http.get(url);
    }
}
