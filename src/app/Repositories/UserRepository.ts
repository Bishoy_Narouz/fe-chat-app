import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserRepository {

    constructor(
        private http: HttpClient,
    ) { }

    hostUrl: string = 'http://localhost:5000/api/';
    apiUrl: string = this.hostUrl + 'User/';

    FindAllFriends(currentUserId) {
        let url = this.apiUrl + "findAllFriends/" + currentUserId;
        return this.http.get(url);
    }
}
